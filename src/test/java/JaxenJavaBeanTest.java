import static org.junit.Assert.*;

import java.util.List;

import org.jaxen.JaxenException;
import org.jaxen.javabean.JavaBeanXPath;
import org.jaxen.saxpath.helpers.XPathReaderFactory;
import org.junit.Test;

public class JaxenJavaBeanTest {

	protected void setUp() throws Exception {
		System.setProperty(XPathReaderFactory.DRIVER_PROPERTY, "");
	}

	@Test
	public void test() throws JaxenException {

		// The position() function does not really have any meaning
		// for JavaBeans, but we know three of them will come before the fourth,
		// even if we don't know which ones.
		// JavaBeanXPath xpath = new JavaBeanXPath(
		// "brother[position()<4]/[age,name]" );
		JavaBeanXPath xpath = new JavaBeanXPath("children/term");

		Expression e = new Expression();
		e.term = "cat";
		And a1 = new And();
		a1.term = "dog";
		e.addChild(a1);
		// Expression e2 = new Expression();
		// e2.term = "dog";
		// a1.addChild(e2);
		System.out.println(e);

		List result = (List) xpath.evaluate(e);
		System.err.println(result);
		// System.out.println(result);
		// assertEquals(3, result.size());

	}

}
