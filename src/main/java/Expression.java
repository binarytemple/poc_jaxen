import java.util.ArrayList;
import java.util.List;

public class Expression {

	Expression parent;
	String term;
	final String type = this.getClass().getName();

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public Expression getParent() {
		return parent;
	}

	public void setParent(Expression parent) {
		this.parent = parent;
	}

	public List<Expression> children = new ArrayList<Expression>();

	public List<Expression> getChildren() {
		return children;
	}

	public void setChildren(List<Expression> children) {
		this.children = children;
	}

	public void addChild(Expression child) {
		child.parent = this;
		this.children.add(child);
	}

//	public String toStringNonRecursive() {
//		return "\n" + this.getClass().getName() + " [parent=" + parent
//				+ ", term=" + term + "]";
//	}

	@Override
	public String toString() {
		return "\n" + type + "[parent="
				+ ((parent != null) ? parent.type : "")
				+ ", term=" + term + ", children=" + children + "]";
	}

}
